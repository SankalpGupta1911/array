const flattens = require("./flatten.cjs")

var _ = require('underscore');

console.log(flattens([1, [2], [3, [[4]]]]))
console.log(flattens([1, [2], [3, [[4]]]],1))
console.log(flattens([1, [2], [3, [[4]]]],2))
console.log(flattens([1, [2], [3, [[4]]]],3))

console.log(flattens([1,[5,8,6,10],"this",["yes",[56,"yupp"]], [2], [3, [[4]]]]))
console.log(flattens([1,[5,8,6,10],"this",["yes",[56,"yupp"]], [2], [3, [[4]]]], 1))
console.log(flattens([1,[5,8,6,10],"this",["yes",[56,"yupp"]], [2], [3, [[4]]]], 2))
console.log(flattens([1,[5,8,6,10],"this",["yes",[56,"yupp"]], [2], [3, [[4]]]], 3))

console.log(flattens([1,3,,5,6]));
console.log(flattens([1,3,,5,6,undefined]));

console.log(_.flatten([1,3,,5,6]));
console.log(_.flatten([1,3,,5,6,undefined]));