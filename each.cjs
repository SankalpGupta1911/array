function each(arr, func) {
    for(let index in arr){
        console.log(func(arr[index],index,arr));
    }
    // Do NOT use forEach to complete this function.
    // Iterates over a list of elements, yielding each in turn to the `cb` function.
    // This only needs to work with arrays.
    // You should also pass the index into `cb` as the second argument
    // based off http://underscorejs.org/#each
}

function square(value,index,list){
    if(!typeof(list[index]=='number' || isNaN(list[index]))) return "";
    return value**2;
}

function double(value,index,list){
    if(!typeof(list[index]=='number' || isNaN(list[index]))) return "";
    return value*2;
}

module.exports = {each, square, double};
