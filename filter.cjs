module.exports = function filter(arr, func){
    let list = []
    for(let index in arr){
        let output=func(arr[index], index ,arr)
        if (output==true){
            list.push(arr[index]);
        }
    }
    return list;
}

