const testcase = require("./arrays.cjs");
const reduce = require("./reduce.cjs")

function intotensum(memo,num){
    return memo + (num*10);
}

console.log(reduce(testcase, intotensum , 0));
console.log(reduce([10,25,56,84,17], function minus5(memo,num){return memo + (num-5);}));

console.log(reduce([10,25,56,84,17], function sum(memo,num){return memo + num}, 0));