module.exports = function find(arr, func){
    for(let element of arr){
        let output=func(element)
        if (output){
            return element;
        }
    }
    return undefined;
    
}