const testcase = require("./arrays.cjs");
const find = require("./find.cjs")

function checkdiv5(num){
    return num % 5 == 0;
}

console.log(find(testcase, checkdiv5));
console.log(find([10,25,56,84,17], function checkeven(num){return num % 2 == 0}));
console.log(find([4,64,78,1,201], checkdiv5));