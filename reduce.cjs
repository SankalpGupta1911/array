module.exports = function reduce(arr, func, memo){
    let startindex = 0;
    if(memo === undefined){
        memo = arr[0];
        startindex=1;
    }
    
    for(let element = startindex; element< arr.length; element++){
        let value = arr[element];
        memo = func(memo, value, element, arr);
    }
    return memo;
}


    // Do NOT use .reduce to complete this function.
    // How reduce works: A reduce function combines all elements into a single value going 
    // from left to right.
    // Elements will be passed one by one into `cb` along with the `startingValue`.
    // `startingValue` should be the first argument passed to `cb` and the array element 
    // should be the second argument.
    // `startingValue` is the starting value.  If `startingValue` is undefined then make 
    // `elements[0]` the initial 