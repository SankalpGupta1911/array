module.exports = function flatten(elements,depth = 1){
    let res = [];

    // To check for a null array, returns a null array
    if(elements.length === 0){return []}

    //To check for a flat array
    let isflat = true;
    for (let element of elements){
        if(Array.isArray(element)){
            isflat = false;
            break;
            //shows that array is not flat 
        }
    }

    //Checking for holes
    let hasHoles = false;
    for(let element of elements){
        if(!(element)){
            hasHoles = true;
        }
    }


    //in the case that array is flat, returns the original array
    if(isflat === true){
        if(hasHoles === true){
            for(let idx=0;idx<elements.length;idx++){
                if(idx in elements){
                    continue;
                }
                else{
                    elements[idx] = undefined;
                }
            }
            return elements;
        }
        return elements;
    }

    for(let element of elements){

        if(!element){
            res.push(undefined);
        }
        else if(Array.isArray(element) && depth>0){
            res = res.concat(flatten(element, depth-1));
        }
        else{
            res.push(element);
        }
    }
    return res;
    // Flattens a nested array (the nesting can be to any depth).
    // Hint: You can solve this using recursion.
    // Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];
}

