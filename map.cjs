module.exports = function maps(arr, cb){
    let list = [];
    for(let idx = 0; idx < arr.length; idx++){
        let value=arr[idx];
        let res = cb(value,idx,arr);
        list.push(res);
    }
    return list;
}

