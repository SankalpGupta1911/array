const filters = require("./filter.cjs");
const testcase = require("./arrays.cjs");

console.log(filters(testcase, function iseven(num){return num % 2 == 0}));

console.log(filters(testcase, function greaterthan3(num){return (num > 3)}));

function checkdiv5(num){
    return num % 5 == 0;
}

console.log(filters(testcase, checkdiv5));